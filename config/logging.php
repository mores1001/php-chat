<?php
return [
    // 默认用哪个
    'default' => env('LOG_CHANNEL', 'stack'),

    'channels' => [
        'socketInfo' => [
            'driver' => 'daily',
            'path' => storage_path('socket/socketInfo.log'),
            'level' => 'info',
            'days' => 7,
        ],
        'socketAction' => [
            'driver' => 'daily',
            'path' => storage_path('socket/socketAction.log'),
            'level' => 'info',
            'days' => 7,
        ],
        // 系统默认，可以合并几个频道，按等级对应记录，符合等级条件的都记录
        'stack' => [
            'driver' => 'stack',
            'channels' => ['single','daily'],
        ],
        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],
        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'info',
            'days' => 7,
            'permission' => 0777,
        ],
    ],
];
