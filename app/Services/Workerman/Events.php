<?php

namespace App\Workerman;

use App\Repositories\UsersRepository;
use App\Repositories\RoomsRepository;
use App\Helpers\AuthHelpers;
use Illuminate\Support\Facades\Log;
use \GatewayWorker\Lib\Gateway;

class Events
{
    public static function onWorkerStart($businessWorker)
    {
    }

    public static function onWebSocketConnect($clientId, $data)
    {
        // 確認是否有傳token
        if (!isset($data['get']['token'])) {
            Gateway::sendToClient($clientId, self::handleMessage(4444));
            Gateway::closeClient($clientId);
            return;
        }

        $token = $data['get']['token'];

        // token 驗證
        /** @var AuthHelpers $AuthVerification */
        $authVerification = app()->make(AuthHelpers::class);
        $result = $authVerification->checkToken($token);

        // 驗證失敗
        if (!$result['result']) {
            Log::channel('socketInfo')->error($clientId . " token驗證失敗");
            Gateway::sendToClient($clientId, self::handleMessage(1001));
            Gateway::closeClient($clientId);
            return;
        }

        $userInfo = $result['info']->toArray();
        $_SESSION['userId'] = $userInfo['id'];
        Gateway::setSession($clientId, $userInfo);

        self::getConnectionPushData($clientId, $userInfo['id']);

        Log::channel('socketInfo')->error($clientId . "「" . $userInfo['id'] . "」登入成功");
    }

    public static function onMessage($clientId, $message)
    {
        // // 確認接收參數為json格式
        $checkJoson = is_string($message) && is_array(json_decode($message,
            true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;

        if (!$checkJoson) {
            Gateway::sendToClient($clientId, self::handleMessage(4444));
            return;
        }

        $content = json_decode($message, true);

        // 確認內容有type
        if (!isset($content['action'])) {
            Gateway::sendToClient($clientId, self::handleMessage(4444));
            return;
        }

        $action = $content['action'];
        $userSession = Gateway::getSession($clientId);

        Log::channel('socketAction')->error($clientId . "「" . $userSession['id'] . "」action「" . $action . "」");

        // 根據type執行
        switch ($action) {
            case 'ping':
                Gateway::sendToClient($clientId, self::handleMessage(0));
                return;
            case 'sendMessage':
                if (!isset($content['roomId']) || empty($content['message'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                self::sendMessage($content['roomId'], $content['message'], $userSession['id'], $clientId);
                break;
            case 'roomList':
                self::getRoomList($clientId, $userSession['id']);
                break;
            case 'roomDetail':
                if (!isset($content['roomId'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                self::getRoomDetail($clientId, $content['roomId']);
                break;
            case 'roomMessage':
                if (!isset($content['roomId']) || !isset($content['timestamp'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                if (!is_integer($content['roomId']) || !is_integer($content['timestamp'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                self::getRoomMessage($clientId, $content['roomId'], $content['timestamp']);
                break;
            case 'createPersonalRoom':
                if (!isset($content['userId']) || !is_integer($content['userId'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                $friendId = $content['userId'];
                if ($userSession['id'] == $friendId) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                self::createPersonalRoom($clientId, $userSession['id'], $friendId);
                break;
            case 'createGroupRoom':
                if (!isset($content['roomName'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }

                if (isset($content['members']) && !is_array($content['members'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                $roomName = $content['roomName'];
                $members = isset($content['members']) ? $content['members'] : [];
                self::createGroupRoom($clientId, $userSession['id'], $roomName, $members);
                break;
            case 'addGroupMember':
                if (!isset($content['members']) || !isset($content['roomId']) || !is_integer($content['roomId']) || !is_array($content['members'])) {
                    Gateway::sendToClient($clientId, self::handleMessage(4444));
                    return;
                }
                self::addGroupMember($clientId, $content['members'], $content['roomId']);
                break;
            default:
                Gateway::sendToClient($clientId, self::handleMessage(4444));
                break;
        }
    }

    public static function onClose()
    {
        /** @var UsersRepository $AuthVerification */
        $usersRepository = app()->make(UsersRepository::class);
        $usersRepository->updateLastTime($_SESSION['id']);

        Log::channel('socketInfo')->error($_SESSION['id'] . " 連線中斷");
    }

    public static function getConnectionPushData($clientId, $userId)
    {
        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $rooms = $roomsRepository->getRoomList($userId);
        $roomIds = [];
        foreach ($rooms as $room) {
            Gateway::joinGroup($clientId, $room['room_id']);
            $roomIds[] = $room['room_id'];
        }

        /** @var UsersRepository $usersRepository */
        $usersRepository = app()->make(UsersRepository::class);
        $lastTime = $usersRepository->getLastTime($userId);
        $messages = $roomsRepository->getMessageByLastTime($roomIds, $lastTime->last_logout);

        $data = [];
        foreach ($messages as $message) {
            if (!array_key_exists($message['room_id'], $data)) {
                $data[$message['room_id']] = [
                    'roomId' => $message['room_id'],
                    'message' => []
                ];
            }
            $data[$message['room_id']]['message'][] = [
                'userId' => $message['user_id'],
                'text' => $message['text'],
                'timestamp' => $message['timestamp'],
            ];
        }
        Gateway::sendToClient($clientId, self::handleMessage(2010, ['rooms' => array_values($data)]));
    }

    private static function getRoomList($clientId, $userId)
    {
        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $data = $roomsRepository->getRoomList($userId)->toArray();
        $result = [];
        foreach ($data as $item) {
            $roomInfo = $item['room_info'];
            $result[] = [
                'roomId' => $roomInfo['id'],
                'type' => $roomInfo['type'],
                'name' => $roomInfo['name'],
                'roomImg' => $roomInfo['room_img'],
                'lastTimestamp' => isset($roomInfo['last_message']) ? $roomInfo['last_message']['timestamp'] : 0,
            ];
        }

        Gateway::sendToClient($clientId, self::handleMessage(2012, ['rooms' => $result]));
    }

    private static function getRoomDetail($clientId, $roomId)
    {
        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $data = $roomsRepository->getRoomDetailById($roomId)->toArray();
        $members = [];

        foreach ($data['members'] as $member) {
            $meberInfo = $member['user_info'];
            $members[] = [
                'userId' => $meberInfo['id'],
                'userName' => $meberInfo['username'],
            ];
        }

        $result = [
            'roomId' => $data['id'],
            'type' => $data['type'],
            'name' => $data['name'],
            'roomImg' => $data['room_img'],
            'createTime' => strtotime($data['created_at']),
            'members' => $members
        ];

        Gateway::sendToClient($clientId, self::handleMessage(2013, $result));
    }

    private static function getRoomMessage($clientId, $roomId, $timestamp)
    {
        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $data = $roomsRepository->getRoomMessageById($roomId, $timestamp)->toArray();

        $result = [];
        foreach ($data as $item) {
            $result[] = [
                'userId' => $item['user_id'],
                'text' => $item['text'],
                'timestamp' => $item['timestamp'],
            ];
        }

        Gateway::sendToClient($clientId, self::handleMessage(2014, ['roomId' => $roomId, 'messages' => $result]));
    }

    private static function sendMessage($roomId, $message, $userId, $clientId)
    {
        $channelList = Gateway::getClientIdListByGroup($roomId);
        if (!array_key_exists($clientId, $channelList)) {
            Gateway::sendToClient($clientId, self::handleMessage(4003));
            return;
        }
        $time = time();

        Gateway::sendToGroup($roomId, self::handleMessage(2011,
            ['roomId' => $roomId, 'message' => $message, 'userId' => $userId, 'timestamp' => $time]));

        /** @var RoomsRepository $roomsRepository */

        $roomsRepository = app()->make(RoomsRepository::class);
        $roomsRepository->saveMessage($roomId, $message, $userId, $time);
    }

    private static function createPersonalRoom($clientId, $userId, $friendId)
    {
        /** @var UsersRepository $roomsRepository */
        $usersRepository = app()->make(UsersRepository::class);
        $checkUsers = $usersRepository->checkUser([$userId, $friendId]);

        if (!$checkUsers['result']) {
            Gateway::sendToClient($clientId, self::handleMessage(4002));
            return;
        }

        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $room = $roomsRepository->createPersonalRoom($userId, $friendId);

        $pushData = self::handleMessage(2015,
            ['roomId' => $room->id, 'createTime' => strtotime($room->created_at), 'members' => $checkUsers['members']]);
        self::joinGroupMembers($room->id, [$userId, $friendId], $pushData);
    }

    private static function createGroupRoom($clientId, $userId, $roomName, $members)
    {
        $members[] = $userId;
        /** @var UsersRepository $roomsRepository */
        $usersRepository = app()->make(UsersRepository::class);
        $checkUsers = $usersRepository->checkUser($members);

        if (!$checkUsers['result']) {
            Gateway::sendToClient($clientId, self::handleMessage(4002));
            return;
        }

        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $room = $roomsRepository->createGroupRoom($userId, $roomName, $members);

        $pushData = self::handleMessage(2016, [
            'roomId' => $room->id,
            'createTime' => strtotime($room->created_at),
            'roomName' => $roomName,
            'members' => $checkUsers['members']
        ]);
        self::joinGroupMembers($room->id, $members, $pushData);
    }

    private static function addGroupMember($clientId, $userIds, $roomId)
    {
        $channelList = Gateway::getClientIdListByGroup($roomId);
        if (!array_key_exists($clientId, $channelList)) {
            Gateway::sendToClient($clientId, self::handleMessage(4003));
            return;
        }

        /** @var UsersRepository $roomsRepository */
        $usersRepository = app()->make(UsersRepository::class);
        $checkUsers = $usersRepository->checkUser($userIds);

        if (!$checkUsers['result']) {
            Gateway::sendToClient($clientId, self::handleMessage(4002));
            return;
        }

        /** @var RoomsRepository $roomsRepository */
        $roomsRepository = app()->make(RoomsRepository::class);
        $roomsRepository->addGrouplUsers($userIds, $roomId);

        foreach ($userIds as $userId) {
            $userInfo = Gateway::getClientIdByUid($userId);
            if (!empty($userInfo)) {
                Gateway::joinGroup($userInfo[0], $roomId);
            }
        }

        Gateway::sendToClient($clientId,
            self::handleMessage(2017, ['roomId' => $roomId, 'addMembers' => $checkUsers['members']]));
        return;
    }

    private static function joinGroupMembers($roomId, $memberIds, $message = null)
    {
        $channelList = Gateway::getAllClientSessions();
        foreach ($channelList as $uid => $member) {
            $userSession = Gateway::getSession($uid);
            if (in_array($userSession['id'], $memberIds)) {
                Gateway::joinGroup($uid, $roomId);
            }
        }

        if (!is_null($message)) {
            Gateway::sendToGroup($roomId, $message);
        }
    }

    private static function handleMessage(int $statusCode, $data = null)
    {
        if (is_null($data)) {
            return json_encode([
                'statusCode' => $statusCode,
            ]);
        }

        return json_encode([
            'statusCode' => $statusCode,
            'data' => $data
        ]);
    }
}
