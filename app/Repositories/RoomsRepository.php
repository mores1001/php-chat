<?php

namespace App\Repositories;

use App\Models\Rooms;
use App\Models\RoomMembers;
use App\Models\Messages;
use Illuminate\Support\Facades\Log;
use PDO;

// use App\Models\RoomUsers;

class RoomsRepository
{
    /** @var Rooms $rooms */
    private $rooms;
    /** @var RoomMembers $roomMembers */
    private $roomMembers;
    /** @var messages $messages */
    private $messages;


    public function __construct(
        Rooms $rooms,
        RoomMembers $roomMembers,
        messages $messages

    ) {
        $this->rooms = $rooms;
        $this->roomMembers = $roomMembers;
        $this->messages = $messages;
    }

    public function getRoomDetailById($RoomId)
    {
        return $this->rooms
            ->select('id', 'type', 'creator', 'name', 'room_img', 'created_at')
            ->where('id', $RoomId)
            ->with('members.userInfo')
            ->first();
    }

    public function createPersonalRoom($userId, $friendId)
    {
        $exist = $this->roomMembers
            ->select('room_members.room_id')
            ->where('room_members.user_id', $userId)
            ->where('room_members.type', 1)
            ->join('room_members as r2', function ($join) use ($friendId) {
                $join->on('room_members.room_id', '=', 'r2.room_id')
                    ->where('r2.type', 1)
                    ->where('r2.user_id', $friendId);
            })
            ->first();

        if (!empty($exist)) {
            return $this->rooms->where('id', $exist->room_id)->first();
        }

        $newRoom = $this->rooms->create([
            'creator' => $userId,
            'type' => 1,
        ]);

        $this->roomMembers->insert(
            [
                [
                    'type' => 1,
                    'user_id' => $userId,
                    'room_id' => $newRoom->id
                ],
                [
                    'type' => 1,
                    'user_id' => $friendId,
                    'room_id' => $newRoom->id
                ],
            ]
        );
        return $newRoom;
    }

    public function createGroupRoom($userId, $roomName, $members)
    {
        $newRoom = $this->rooms->create([
            'creator' => $userId,
            'name' => $roomName,
            'type' => 2,
        ]);

        $userData = [];
        foreach ($members as $member) {
            $userData[] = [
                'type' => 2,
                'user_id' => $member,
                'room_id' => $newRoom->id
            ];
        }

        $this->roomMembers->insert($userData);

        return $newRoom;
    }

    public function addGrouplUsers($users, $roomId)
    {
        $insertData = [];
        foreach ($users as $user) {
            $memberExits = $this->roomMembers
                ->where('type', 2)
                ->where('user_id', $user)
                ->where('room_id', $roomId)
                ->first();
            if (!empty($memberExits)) {
                continue;
            }

            $insertData[] = [
                'user_id' => $user,
                'room_id' => $roomId,
                'type' => 2,
            ];
        }

        $this->roomMembers->insert($insertData);
    }

    public function getRoomList($userId)
    {
        return $this->roomMembers
            ->select('room_id')
            ->where('user_id', $userId)
            ->with('roomInfo.lastMessage')
            ->get();
    }

    public function saveMessage($roomId, $message, $userId, $time)
    {
        $existRoom =
            $this->rooms->where('id', $roomId)
            ->first();

        if (empty($existRoom)) {
            Log::channel('socketSendMessage')->error('找無room id: ' . $roomId . ' 來自user id: ' . $userId);
        }

        $this->messages->create([
            'room_id' => $roomId,
            'user_id' => $userId,
            'text' => $message,
            'timestamp' => $time
        ]);
    }

    public function getRoomMessageById($roomId, $timestamp)
    {
        return $this->messages
            ->select('user_id', 'text', 'timestamp')
            ->where('room_id', $roomId)
            ->where('timestamp', '>=', $timestamp)
            ->get();
    }

    public function getMessageByLastTime($roomIds, $lastTime)
    {
        return $this->messages
            ->select('room_id', 'user_id', 'text', 'timestamp')
            ->whereIn('room_id', $roomIds)
            ->where('timestamp', '>', $lastTime)
            ->get();
    }
}
