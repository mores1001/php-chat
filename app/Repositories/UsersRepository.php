<?php

namespace App\Repositories;


use App\Models\Users;

class UsersRepository
{
    /** @var Users $users */
    private $users;

    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    public function searchUserByUserName($username)
    {
        return $this->users->where('username', $username)->first();
    }

    public function getLastTime($userId)
    {
        return $this->users
            ->select('id', 'last_logout')
            ->where('id', $userId)
            ->first();
    }

    public function updateLastTime($userId)
    {
        return $this->users
            ->where('id', $userId)
            ->update([
                'last_logout' => time(),
            ]);
    }

    public function checkUser($userIds)
    {
        $members = [];
        foreach ($userIds as $userId) {
            $userExits = $this->users
                ->where('id', $userId)
                ->first();

            if (empty($userExits)) {
                return ['result' => false];
            }

            if (array_key_exists($userId, $members)) {
                continue;
            }

            $members[$userId] = [
                'userId' => $userExits->id,
                'userName' => $userExits->username,
            ];
        }

        return ['result' => true, 'members' => array_values($members)];
    }
}
