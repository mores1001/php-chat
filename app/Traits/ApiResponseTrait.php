<?php

namespace App\Traits;

trait ApiResponseTrait
{
    public function apiResponse($message, $code, $data)
    {
        return response()
            ->json([
                'message' => (string)$message,
                'statusCode' => (int)$code,
                'data' => $data
            ]);
    }
}
