<?php

namespace App\Http\Controllers;


use App\Repositories\UsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /** @var UsersRepository $usersRepository */
    private $usersRepository;
    /** @var JWTAuth $JWTAuth */
    private $JWTAuth;

    public function __construct(JWTAuth $JWTAuth, UsersRepository $usersRepository)
    {

        $this->JWTAuth = $JWTAuth;
        $this->usersRepository = $usersRepository;
    }

    /**
     * 登入
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        if (empty($username) || empty($password)) {
            return $this->apiResponse('account required.', 4444, (object)[]);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
        ]);
        /** 帳號不存在 */
        if (!$validator->fails()) {
            return $this->apiResponse('Authentication failed', 1002, (object)[]);
        }

        /** 驗證失敗 */
        if (!$token = $this->JWTAuth->attempt([
            'username' => strtolower($request->get('username')),
            'password' => $request->get('password'),
        ])) {
            return $this->apiResponse('Authentication failed', 1002, (object)[]);
        }
        $user = [
            'dateOfRegistration' => \Carbon\Carbon::now()->timestamp,
            'token' => $token,
            'userId' => Auth::user()->id,
            'username' => Auth::user()->username
        ];
        return $this->apiResponse('', 0, $user);
    }

    /**
     * 搜尋使用者
     * @param $username
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchUser($username)
    {
        $data = $this->usersRepository->searchUserByUserName($username);
        if (is_null($data)) {
            return $this->apiResponse('', 0, (object)[]);
        }
        return $this->apiResponse('success', 0, [
            'userId' => $data->id,
            'username' => $data->username,
        ]);
    }
}
