<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $room_id
 * @property int $user_id
 * @property string $text
 * @property int $timestamp
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class Messages extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['room_id', 'user_id', 'text', 'timestamp', 'deleted_at', 'created_at', 'updated_at'];

}
