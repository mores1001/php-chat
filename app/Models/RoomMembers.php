<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $user_id
 * @property integer $room_id
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class RoomMembers extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['type', 'user_id', 'room_id', 'deleted_at', 'created_at', 'updated_at'];

    public function roomInfo()
    {
        return $this->belongsTo('App\Models\Rooms', 'room_id', 'id');
    }

    public function userInfo()
    {
        return $this->belongsTo('App\Models\Users', 'user_id', 'id');
    }
}
