<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property boolean $type
 * @property int $creator
 * @property string $name
 * @property string $room_img
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 */
class Rooms extends Model
{
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['id', 'type', 'creator', 'name', 'room_img', 'deleted_at', 'created_at', 'updated_at'];

    public function lastMessage()
    {
        return $this->hasOne('App\Models\Messages', 'room_id', 'id')
            ->orderBy('created_at', 'DESC');
    }

    public function members()
    {
        return $this->hasMany('App\Models\RoomMembers', 'room_id', 'id');
    }
}
