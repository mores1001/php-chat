<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Tymon\JWTAuth\JWTAuth;
use App\Models\Guests;
use Illuminate\Support\Facades\Auth;

class AuthHelpers
{
    protected $JWTAuth;

    public function __construct(JWTAuth $jwt)
    {
        $this->JWTAuth = $jwt;
    }

    public function checkToken($token)
    {
        $result = [
            'result' => false,
            'info' => []
        ];

        try {
            $this->JWTAuth->setToken($token);
            if($this->JWTAuth->check()){
                $result['result'] = true;
                $result['info'] = $this->JWTAuth->authenticate();
                $result['payload'] = $this->JWTAuth->getPayload()->toArray();
                return $result;
            }
            return $result;

        } catch (\Exception $e) {
            Log::channel('socket')->error("token驗證失敗");
            Log::channel('socket')->error($e);
            return $result;
        }
    }
}