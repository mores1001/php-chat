<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'username' => 'Andy',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'Vioops',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'JoJo',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'Danny',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'Tim',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'John',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
            [
                'username' => 'Frank',
                'password' => Hash::make('a111111'),
                'last_logout' => time()
            ],
        ];

        DB::table('users')->insert($users);
    }
}